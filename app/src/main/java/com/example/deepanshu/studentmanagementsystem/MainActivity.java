package com.example.deepanshu.studentmanagementsystem;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepanshu.DataBase.DBController;
import com.example.deepanshu.Entities.DrawerAdapter;
import com.example.deepanshu.Entities.DrawerBlock;
import com.example.deepanshu.Entities.MyRecyclerAdapter;
import com.example.deepanshu.Entities.Student;
import com.example.deepanshu.Utilities.Conventions;
import com.example.deepanshu.Utilities.EndFragmentInterface;
import com.example.deepanshu.Utilities.MyOnItemClickListener;
import com.example.deepanshu.Utilities.StartFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements EndFragmentInterface, Conventions, View.OnClickListener, MyOnItemClickListener {

    public static int position = 0;
    private RecyclerView recyclerDataView;
    private Button listView, gridView;
    private AddStudent stuPage;
    private MyRecyclerAdapter viewDataAdap;
    private Dialog dialogOperations;
    private Spinner sortSpinner;
    private ArrayAdapter<String> sortByAdap;
    private int sortBy = SORT_BY_ID;
    private Student tempStu;
    private ArrayList<Student> stuDataBase = new ArrayList<>();
    private android.support.v4.app.FragmentManager fragmentManager;
    private RelativeLayout primaryLayout;
    private android.support.v4.widget.DrawerLayout mDrawerLayout;
    private MyAsyncTasks viewAllTask;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerAdapter drawerAdapter;
    private ArrayList<DrawerBlock> drawerBlockArrayList = new ArrayList<>();
    private ListView drawerListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setOnClick();
        showData(LIST_VIEW);
    }


    public void init() {
        //Setting objects from the screen
        recyclerDataView = (RecyclerView) findViewById(R.id.recyclerDataView);
        listView = (Button) findViewById(R.id.btListView);
        gridView = (Button) findViewById(R.id.btGridView);
        primaryLayout = (RelativeLayout) findViewById(R.id.primaryLayout);
        sortSpinner = (Spinner) findViewById(R.id.spinner);
        mDrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout_base);
        drawerListView = (ListView) findViewById(R.id.drawer_list);
        viewDataAdap = new MyRecyclerAdapter(this, stuDataBase);
        recyclerDataView.setAdapter(viewDataAdap);
        sortByAdap = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.sortby));
        sortSpinner.setAdapter(sortByAdap);
        viewAllTask = new MyAsyncTasks();
        //show all data to the recycler view
        viewAllTask.execute(VIEW_ALL_STUDENT);
        //add the items to fragment i.e. logout button and add student fragment
        drawerBlockArrayList.add(new DrawerBlock(R.drawable.ic_navigation_drawer, "Add Student"));
        drawerBlockArrayList.add(new DrawerBlock(R.drawable.ic_navigation_drawer, "Logout"));

        drawerAdapter = new DrawerAdapter(drawerBlockArrayList, getApplicationContext());
        drawerListView.setAdapter(drawerAdapter);
    }

    public void setOnClick() {
        listView.setOnClickListener(MainActivity.this);
        gridView.setOnClickListener(MainActivity.this);
//To sort the data according to
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int sortPosition, long id) {
                if (sortBy != sortPosition) {
                    sortBy = sortPosition;
                    if (viewAllTask.getStatus() == AsyncTask.Status.RUNNING)
                        viewAllTask.cancel(true);
                    viewAllTask = new MyAsyncTasks();
                    viewAllTask.execute(VIEW_ALL_STUDENT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//To logout or to go to add_new_student_page
        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0://for Adding a new student
                        fragShow();
                        new MyAsyncTasks().execute(NEW_STUDENT);
                        primaryLayout.setVisibility(View.GONE);
                        getSupportActionBar().setTitle("NEW_STUDENT");
                        mDrawerLayout.closeDrawer(drawerListView);
                        break;
                    case 1://for logout
                        SharedPreferences.Editor edit = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE).edit();
                        edit.clear().commit();
                        Intent intentLogin = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intentLogin);
                        getSupportActionBar().setTitle("Logout");
                        mDrawerLayout.closeDrawers();
                        finish();
                        break;

                }
            }
        });
        // to open and close navigation drawer on swipe or click
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();

            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        viewDataAdap.setMyOnItemClickListener(MainActivity.this);

    }

    public void showData(int choice) {
        //to show data in grid form or list form
        switch (choice) {
            case GRID_VIEW:
                listView.setFocusableInTouchMode(false);
                gridView.setFocusableInTouchMode(true);
                recyclerDataView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                break;
            case LIST_VIEW:
            default:
                listView.setFocusableInTouchMode(true);
                gridView.setFocusableInTouchMode(false);
                recyclerDataView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                break;
        }
        viewDataAdap.notifyDataSetChanged();

    }

    public void showDialog() {
        //custom Dialog for view,update,delete
        dialogOperations = new Dialog(MainActivity.this);
        dialogOperations.setContentView(R.layout.dialog_layout);
        Button viewBT = (Button) dialogOperations.findViewById(R.id.viewBT);
        Button updateBT = (Button) dialogOperations.findViewById(R.id.updateBT);
        Button deleteBT = (Button) dialogOperations.findViewById(R.id.deleteBT);
        dialogOperations.setTitle("Operations");
        updateBT.setOnClickListener(MainActivity.this);
        deleteBT.setOnClickListener(MainActivity.this);
        viewBT.setOnClickListener(MainActivity.this);
        dialogOperations.show();
    }

    /* to lock the navigation drawer while on fragment */
    void lockNavigationBar() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerLayout.closeDrawer(drawerListView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    /* to unlock the navigation drawer when coming back from fragment*/
    void unlockNavigationBar() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btListView:
                showData(LIST_VIEW);
                break;
            case R.id.btGridView:
                showData(GRID_VIEW);
                break;
            case R.id.viewBT:
                dialogOperations.dismiss();
                fragShow();
                new MyAsyncTasks().execute(VIEW_SPECIFIC_STUDENT);
                primaryLayout.setVisibility(View.GONE);
                break;
            case R.id.updateBT:
                dialogOperations.dismiss();
                fragShow();
                new MyAsyncTasks().execute(UPDATE_SPECIFIC_STUDENT);
                primaryLayout.setVisibility(View.GONE);
                break;
            case R.id.deleteBT:
                dialogOperations.dismiss();
                new MyAsyncTasks().execute(DELETE_STUDENT);
                break;

            default:
                break;
        }

    }

    public void showToast(String data) {//to show any message
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemClick(int position) {//to get the index of the item clicked and show dialogbox accordingly
        this.position = position;
        showDialog();
    }

    public void fragHide() {
        //to remove the add student fragment
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().remove(stuPage).commit();
            primaryLayout.setVisibility(View.VISIBLE);
        }
        hideKeybord();
    }

    public void fragShow() {
//to start the fragment
        stuPage = new AddStudent();
        fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragTransc = fragmentManager.beginTransaction();
        fragTransc.add(R.id.mainPage, stuPage, "StudentFragment");
        fragTransc.commit();

    }

    class MyAsyncTasks extends AsyncTask<Integer, Student, Long> {
        private MyRecyclerAdapter myRecyclerAdapter;
        private DBController dbExec;
        private long result = -2;
        private Student tempStu1;
        private int choice, newId;
        ProgressDialog progDialog;
        StartFragment sf;

        @Override
        protected void onPreExecute() {

            progDialog = new ProgressDialog(MainActivity.this);
            progDialog.show();
            progDialog.setMessage(LOADING);
            myRecyclerAdapter = (MyRecyclerAdapter) recyclerDataView.getAdapter();
            dbExec = new DBController(MainActivity.this);
            if (dialogOperations != null && dialogOperations.isShowing())
                dialogOperations.dismiss();
        }


        @Override
        protected Long doInBackground(Integer... params) {
            this.choice = params[0];
            switch (choice) {
                case VIEW_ALL_STUDENT:
                    stuDataBase = dbExec.viewAllStudents(sortBy, stuDataBase);
                    break;
                case UPDATE_SPECIFIC_STUDENT:
                    tempStu1 = dbExec.viewStudent(position);
                    break;
                case VIEW_SPECIFIC_STUDENT:
                    tempStu1 = dbExec.viewStudent(position);
                    break;
                case DELETE_STUDENT:
                    result = dbExec.deleteStudent(position);
                    break;
                case UPDATE_STUDENT:
                    result = dbExec.updateStudent(tempStu, tempStu.getId());
                    break;
                case NEW_STUDENT:
                    newId = dbExec.newId();
                    break;
                case INSERT_STUDENT:
                    result = dbExec.insertStudent(tempStu);
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long integer) {
            if (fragmentManager != null) {
                sf = (StartFragment) fragmentManager.findFragmentByTag("StudentFragment");
            }
            if (sf != null) {
                switch (choice) {
                    case UPDATE_SPECIFIC_STUDENT:
                        sf.btChoice(DIALOG_EDIT, tempStu1, 0);
                        lockNavigationBar();
                        break;
                    case NEW_STUDENT:
                        sf.btChoice(ADD_STUDENT, null, newId);
                        lockNavigationBar();
                        break;
                    case VIEW_SPECIFIC_STUDENT:
                        sf.btChoice(DIALOG_VIEW, tempStu1, 0);
                        lockNavigationBar();

                        break;
                }
            }
            if (result > 0) {//to check wether the query is executed or not
                showToast(OPERATION_SUCCESSFUL);
                if (viewAllTask.getStatus() == AsyncTask.Status.RUNNING)
                    viewAllTask.cancel(true);
                viewAllTask = new MyAsyncTasks();
                viewAllTask.execute(VIEW_ALL_STUDENT);
            } else if (result == -1)
                showToast(OPERATION_UNSUCCESSFUL);
            myRecyclerAdapter.notifyDataSetChanged();
            progDialog.dismiss();
        }

    }

    @Override
    public void endFragment(int todo, Student tempStu) {//for returning to activity from fragment
        fragHide();
        unlockNavigationBar();
        this.tempStu = tempStu;
        switch (todo) {
            case INSERT_STUDENT:
                new MyAsyncTasks().execute(INSERT_STUDENT);
                break;
            case UPDATE_STUDENT:
                new MyAsyncTasks().execute(UPDATE_STUDENT);
                break;
        }
        viewDataAdap.notifyDataSetChanged();
    }
    public void hideKeybord() {
//to hide keyboard if shown
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
