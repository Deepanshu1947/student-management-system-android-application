package com.example.deepanshu.studentmanagementsystem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.deepanshu.Utilities.Conventions;
import com.example.deepanshu.Utilities.DataBaseConventions;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, DataBaseConventions, Conventions {
    Button btLogin, btCancel;
    EditText etUserName, etPassword;
    SharedPreferences sharedPref;
    String uName, uPwd;
    Intent welcomeAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        setOnClick();
        checkSharedPref();
    }

    public void checkSharedPref() {
        //to match the login credentials with the stored username and password
        if (uName.equalsIgnoreCase(TEMP_UNAME) && uPwd.equalsIgnoreCase(TEMP_PWD)) {
            startActivity(welcomeAct);
            finish();
        }
    }

    public void init() {
        btLogin = (Button) findViewById(R.id.btLogin);
        btCancel = (Button) findViewById(R.id.btCancel);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        sharedPref = this.getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        welcomeAct = new Intent(LoginActivity.this, MainActivity.class);
        uName = sharedPref.getString("uName", "");
        uPwd = sharedPref.getString("uPwd", "");
    }

    public void setOnClick() {
        btLogin.setOnClickListener(LoginActivity.this);
        btCancel.setOnClickListener(LoginActivity.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btLogin:
                //to save the login credentials for next time in cache
                uName = String.valueOf(etUserName.getText());
                uPwd = String.valueOf(etPassword.getText());
                if (uName.equalsIgnoreCase(TEMP_UNAME) && uPwd.equalsIgnoreCase(TEMP_PWD)) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("uName", uName);
                    editor.putString("uPwd", uPwd);
                    editor.commit();
                    startActivity(welcomeAct);
                    finish();
                }
                break;
            case R.id.btCancel:
                etPassword.setText(null);
                etUserName.setText(null);
                break;
        }
    }

}
