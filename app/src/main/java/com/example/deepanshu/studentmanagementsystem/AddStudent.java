package com.example.deepanshu.studentmanagementsystem;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepanshu.Entities.Student;
import com.example.deepanshu.Utilities.Conventions;
import com.example.deepanshu.Utilities.DataBaseConventions;
import com.example.deepanshu.Utilities.EndFragmentInterface;
import com.example.deepanshu.Utilities.StartFragment;
import com.example.deepanshu.Utilities.Validations;

public class AddStudent extends Fragment implements View.OnClickListener, StartFragment, DataBaseConventions, Conventions {
    EditText firstNameEdit, lastNameEdit, addressEdit, mobileNoEdit;
    TextView setIdEdit;
    Button saveButton, cancelButton;
    Activity currActivity;
    Student tempStu;
    Validations check;
    EndFragmentInterface closeFrag;
    View myView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setOnClick();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.activity_add_student, container, false);
        return myView;
    }


    public void init() {
        currActivity = getActivity();
        setIdEdit = (TextView) myView.findViewById(R.id.setIdEdit);
        firstNameEdit = (EditText) myView.findViewById(R.id.firstNameEdit);
        lastNameEdit = (EditText) myView.findViewById(R.id.lastNameEdit);
        addressEdit = (EditText) myView.findViewById(R.id.addressEdit);
        mobileNoEdit = (EditText) myView.findViewById(R.id.mobNoEdit);
        cancelButton = (Button) myView.findViewById(R.id.cancelButton);
        saveButton = (Button) myView.findViewById(R.id.saveButton);
        check = new Validations();
        closeFrag = (EndFragmentInterface) currActivity;//downcasting the end fragment object to call it when ending this fragment
    }

    public void setOnClick() {
        cancelButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    public void viewStudent() {//to view the student object recieved for view and update
        if (tempStu == null) {
            showToast("No Student to View ");
        } else {
            setIdEdit.setText(String.valueOf(tempStu.getId()));
            firstNameEdit.setText(tempStu.getFName());
            lastNameEdit.setText(tempStu.getLName());
            addressEdit.setText(tempStu.getAddress());
            mobileNoEdit.setText(tempStu.getMobileNumber());
        }
    }

    public void setNonEditable() {//for setting the fields non editable during view
        saveButton.setVisibility(View.GONE);
        firstNameEdit.setKeyListener(null);
        lastNameEdit.setKeyListener(null);
        addressEdit.setKeyListener(null);
        mobileNoEdit.setKeyListener(null);
    }


    public Student getDataFromScreen() {//getting the new data entered by the user
        Student newStudent = new Student();
        newStudent.setId(Integer.parseInt((String) setIdEdit.getText()));
        newStudent.setFName(firstNameEdit.getText().toString());
        newStudent.setLName(lastNameEdit.getText().toString());
        newStudent.setAddress(addressEdit.getText().toString());
        newStudent.setMobileNumber(mobileNoEdit.getText().toString());
        return newStudent;
    }

    public void showToast(String data) {
        Toast.makeText(currActivity, data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        tempStu = getDataFromScreen();
        switch (v.getId()) {
            case R.id.saveButton:
                switch (check.dataValidate(tempStu)) {//checking the validations
                    case IN_VALID_PH_NO:
                        showToast(IN_VALID_PH_NO_STR);
                        break;
                    case IN_VALID_DATA:
                        showToast(IN_VALID_DATA_STR);
                        break;
                    case IN_VALID_NAME:
                        showToast(IN_VALID_NAME_STR);
                        break;
                    case IN_VALID_ADDRESS:
                        showToast(IN_VALID_ADDRESS_STR);
                        break;
                    case VALID_DATA:
                        if (saveButton.getText().toString().equalsIgnoreCase("save")) {
                            closeFrag.endFragment(INSERT_STUDENT, tempStu);
                        } else if (saveButton.getText().toString().equalsIgnoreCase("update")) {
                            closeFrag.endFragment(UPDATE_STUDENT, tempStu);
                        }
                        break;
                }
                break;
            case R.id.cancelButton:
                closeFrag.endFragment(0, null);

                break;
        }

    }


    @Override
    public void btChoice(int toDo, Student tempStu, int newId) {
        // check for what the fragment has been started and getting the data if it is for view and update
        this.tempStu = tempStu;
        switch (toDo) {
            case ADD_STUDENT:
                setIdEdit.setText(String.valueOf(newId));
                saveButton.setText(SAVE);
                break;
            case DIALOG_EDIT:
                viewStudent();
                saveButton.setText(UPDATE);

                break;
            case DIALOG_VIEW:
                setNonEditable();
                viewStudent();
                break;

        }
    }
}