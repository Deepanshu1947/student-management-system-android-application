package com.example.deepanshu.Entities;

/**
 * Created by Deepanshu on 10/1/2015.
 */
public class DrawerBlock {
    //type of one drawer item
    int icon;
    String title;
    public DrawerBlock( int icon,String title)
    {
        this.icon=icon;
        this.title=title;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }
}
