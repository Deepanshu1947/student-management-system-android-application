package com.example.deepanshu.Entities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deepanshu.studentmanagementsystem.MainActivity;
import com.example.deepanshu.studentmanagementsystem.R;

import java.util.ArrayList;

/**
 * Created by Deepanshu on 10/1/2015.
 */
public class DrawerAdapter extends BaseAdapter {
    //adapter for navigation drawer
    ArrayList<DrawerBlock> arrayList;
    Context mContext;
    public  DrawerAdapter(ArrayList<DrawerBlock> arrList,Context cont)
    {
        mContext=cont;
        arrayList=arrList;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=View.inflate(mContext, R.layout.drawer_layout,null);
        ImageView imageView=(ImageView) convertView.findViewById(R.id.drawer_image_view);
        TextView btAction=(TextView) convertView.findViewById(R.id.drawer_textview);
        imageView.setImageResource(arrayList.get(position).getIcon());
        btAction.setText(arrayList.get(position).getTitle());
        return convertView;
    }
}
