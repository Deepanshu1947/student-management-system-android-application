package com.example.deepanshu.Entities;

import java.io.Serializable;

/**
 * Created by Deepanshu on 9/8/2015.
 */
public class Student implements Serializable {
    //student type as serializable so that it can be passed between fragments through intent
    private String fName, lName, address, mobileNumber;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFName() {
        return fName;
    }

    public String getLName() {
        return lName;
    }

    public String getAddress() {
        return address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setLName(String name) {
        this.lName = name;
    }

    public void setFName(String name) {
        this.fName = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }


}
