package com.example.deepanshu.Entities;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.deepanshu.DataBase.DBHelper;
import com.example.deepanshu.Utilities.DataBaseConventions;
import com.example.deepanshu.Utilities.MyOnItemClickListener;
import com.example.deepanshu.studentmanagementsystem.R;

import java.util.ArrayList;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> implements DataBaseConventions {

    LayoutInflater myLayoutInflater;
    ArrayList<Student> stuDataBase;

    MyOnItemClickListener myOnItemClickListener;

    public MyRecyclerAdapter(Context context, ArrayList<Student> studataBase) {
        myLayoutInflater = LayoutInflater.from(context);
        this.stuDataBase = studataBase;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = myLayoutInflater.inflate(R.layout.block_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        viewHolder.idTV.setText(String.valueOf(stuDataBase.get(i).getId()));
        viewHolder.nameTV.setText(stuDataBase.get(i).getFName());
        viewHolder.blockLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOnItemClickListener.onItemClick(stuDataBase.get(i).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return stuDataBase.size();
    }

    public void setMyOnItemClickListener(MyOnItemClickListener myOnItemClickListener) {
        this.myOnItemClickListener = myOnItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView idTV, nameTV;
        LinearLayout blockLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            blockLayout = (LinearLayout) itemView.findViewById(R.id.blockLayout);
            idTV = (TextView) itemView.findViewById(R.id.label_id_show);
            nameTV = (TextView) itemView.findViewById(R.id.label_name_show);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }
}
