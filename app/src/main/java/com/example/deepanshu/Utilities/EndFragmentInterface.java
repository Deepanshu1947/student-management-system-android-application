package com.example.deepanshu.Utilities;

import com.example.deepanshu.Entities.Student;

/**
 * Created by Deepanshu on 9/24/2015.
 */
public interface EndFragmentInterface {

    void endFragment(int toDo, Student tempStu);

}
