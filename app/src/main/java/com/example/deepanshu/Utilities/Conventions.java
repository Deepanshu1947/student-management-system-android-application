package com.example.deepanshu.Utilities;


/**
 * Created by Deepanshu on 9/8/2015.
 */
public interface Conventions {
    //general application constants
    int MAIN_PAGE = 1,
            ADD_STUDENT = 1,
            ADD_STU_PAGE = 2,
            DIALOG_BOX = 3,
            DIALOG_VIEW = 2,
            DIALOG_EDIT = 3,
            GRID_VIEW = 1,
            LIST_VIEW = 2,
            IN_VALID_NAME = 1,
            IN_VALID_ADDRESS = 2,
            IN_VALID_PH_NO = 3,
            IN_VALID_DATA = 4,
            VALID_DATA = 5,
            VIEW_ALL_STUDENT = 11,
            VIEW_SPECIFIC_STUDENT = 12,
            DELETE_STUDENT = 13,
            UPDATE_STUDENT = 14,
            INSERT_STUDENT = 15,
            NEW_STUDENT = 16,
            UPDATE_SPECIFIC_STUDENT = 17,
            SORT_BY_ID = 0,
            SORT_BY_NAME = 1;
    String LOGIN_PREF = "LoginPref",
            OPERATION_SUCCESSFUL = "Success",
            OPERATION_UNSUCCESSFUL = "Error",
            LOADING = "Performing",
            SAVE = "SAVE",
            UPDATE = "UPDATE",
            IN_VALID_DATA_STR = "ENTER COMPLETE INFORMATION",
            NUMBER_STRING_FORMAT = "^[0-9]{10}$",
            NAME_STRING_FORMAT = "[a-zA-Z]{5,32}$",
            IN_VALID_NAME_STR = "ENTER VALID NAME",
            IN_VALID_ADDRESS_STR = "ENTER VALID ADDRESS",
            IN_VALID_PH_NO_STR = "ENTER VALID PHONE NUMBER";
    Boolean
            VALID = true,
            IN_VALID = false;

}
