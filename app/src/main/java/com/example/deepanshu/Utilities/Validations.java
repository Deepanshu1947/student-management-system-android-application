package com.example.deepanshu.Utilities;


import com.example.deepanshu.Entities.Student;

public class Validations implements Conventions {

    public int dataValidate(Student tempStudent) {
        if (!isNonEmptyStudent(tempStudent))
            return IN_VALID_DATA;
        else if (!isName(tempStudent.getFName(), tempStudent.getLName()))
            return IN_VALID_NAME;
        else if (!isPhoneValidate(tempStudent.getMobileNumber()))
            return IN_VALID_PH_NO;
        return VALID_DATA;
    }

    public Boolean isName(String fName, String lName) {
        if (fName.matches(NAME_STRING_FORMAT)
                && lName.matches(NAME_STRING_FORMAT))
            return VALID;
        else
            return IN_VALID;
    }

    public Boolean isPhoneValidate(String number) {
        if (number.matches(NUMBER_STRING_FORMAT))
            return VALID;
        else
            return IN_VALID;
    }

    public Boolean isNonEmptyStudent(Student newStudent) {
        if (!newStudent.getFName().isEmpty()
                && !newStudent.getLName().isEmpty()
                && !newStudent.getAddress().isEmpty()
                && !newStudent.getMobileNumber().isEmpty())
            return VALID;
        return IN_VALID;
    }

}
