package com.example.deepanshu.Utilities;

/**
 * Created by Deepanshu on 9/16/2015.
 */
public interface MyOnItemClickListener {
    void onItemClick(int position);
}
