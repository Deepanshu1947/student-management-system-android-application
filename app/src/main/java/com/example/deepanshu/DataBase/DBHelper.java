package com.example.deepanshu.DataBase;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.deepanshu.Utilities.Conventions;
import com.example.deepanshu.Utilities.DataBaseConventions;

/**
 * Created by Deepanshu on 9/15/2015.
 */
public class DBHelper extends SQLiteOpenHelper implements DataBaseConventions, Conventions {

    public DBHelper(Context context1, int version) {
        super(context1, DB_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE "
                    + TB_NAME + " ("
                    + UID + " INTEGER PRIMARY KEY , "
                    + U_FNAME + " VARCHAR(32),"
                    + U_LNAME + " VARCHAR(32),"
                    + U_MOBILE_NUM + " VARCHAR(10),"
                    + U_ADDRESS + " VARCHAR(255)" + ")");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE  IF EXISTS " + TB_NAME);
            onCreate(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
