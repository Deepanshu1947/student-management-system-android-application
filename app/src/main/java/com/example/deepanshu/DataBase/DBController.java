package com.example.deepanshu.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.deepanshu.Entities.Student;
import com.example.deepanshu.Utilities.Conventions;
import com.example.deepanshu.Utilities.DataBaseConventions;

import java.util.ArrayList;

/**
 * Created by Deepanshu on 9/15/2015.
 */
public class DBController implements DataBaseConventions, Conventions {
    Context context;
    DBHelper dbHelper;
    SQLiteDatabase sqliteDataBase;
    Cursor cursor;

    public DBController(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context, DATABASE_VERSION);
    }

    public long insertStudent(Student student) {
        dbOpenWrite();
        ContentValues contentValues = new ContentValues();
        contentValues.put(U_FNAME, student.getFName());
        contentValues.put(U_LNAME, student.getLName());
        contentValues.put(U_ADDRESS, student.getAddress());
        contentValues.put(U_MOBILE_NUM, student.getMobileNumber());
        long id = sqliteDataBase.insert(TB_NAME, null, contentValues);
        dbClose();
        return id;
    }

    public long updateStudent(Student student, int id) {
        dbOpenWrite();
        ContentValues contentValues = new ContentValues();
        contentValues.put(U_FNAME, student.getFName());
        contentValues.put(U_LNAME, student.getLName());
        contentValues.put(U_ADDRESS, student.getAddress());
        contentValues.put(U_MOBILE_NUM, student.getMobileNumber());
        long result = sqliteDataBase.update(TB_NAME, contentValues, UID + "=?", new String[]{String.valueOf(id)});
        dbClose();
        return result;
    }

    public long deleteStudent(int idDelete) {
        dbOpenWrite();
        long id;
        String[] str = {String.valueOf(idDelete)};
        id = sqliteDataBase.delete(TB_NAME, UID + "=?", str);
        dbClose();
        return id;
    }

    public Student viewStudent(int id) {
        dbOpenWrite();
        Student tempStu = null;
        String[] strFind = {UID, U_FNAME, U_LNAME, U_MOBILE_NUM, U_ADDRESS};
        cursor = sqliteDataBase.query(TB_NAME, strFind, UID + "=?", new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            tempStu = new Student();
            tempStu.setId(cursor.getInt(0));
            tempStu.setFName(cursor.getString(1));
            tempStu.setLName(cursor.getString(2));
            tempStu.setMobileNumber(cursor.getString(3));
            tempStu.setAddress(cursor.getString(4));
        }
        dbClose();
        return tempStu;
    }

    public ArrayList<Student> viewAllStudents(int choice, ArrayList<Student> stuDatabase) {
        dbOpenWrite();
        stuDatabase.clear();
        String ordBy;
        switch (choice) {
            case SORT_BY_ID:
                ordBy = UID;
                break;
            case SORT_BY_NAME:
            default:
                ordBy = U_FNAME;
        }
        String[] strFind = {UID, U_FNAME, U_LNAME, U_ADDRESS, U_MOBILE_NUM};
        cursor = sqliteDataBase.query(TB_NAME, strFind, null, null, null, null, ordBy);
        while (cursor.moveToNext()) {
            Student tempStu = new Student();
            tempStu.setId(cursor.getInt(0));
            tempStu.setFName(cursor.getString(1));
            tempStu.setLName(cursor.getString(2));
            tempStu.setMobileNumber(cursor.getString(4));
            tempStu.setAddress(cursor.getString(3));
            stuDatabase.add(tempStu);
        }

        dbClose();

        return stuDatabase;
    }

    public int newId() {
        int id = 0;
        dbOpenRead();
        cursor = sqliteDataBase.rawQuery("Select * from" + TB_NAME, null);
        if (cursor.moveToFirst()) {
            cursor.moveToLast();
            id = cursor.getInt(0);
        }
        id++;
        dbClose();
        return id;
    }

    public void dbOpenRead() {
        sqliteDataBase = dbHelper.getReadableDatabase();
    }

    public void dbOpenWrite() {
        sqliteDataBase = dbHelper.getWritableDatabase();
    }

    public void dbClose() {
        sqliteDataBase.close();
    }
}
